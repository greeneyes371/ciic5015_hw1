from agents import *

class MyTrivialVacuumEnvironment(Environment):
    def __init__(self):
        super().__init__()

        self.status = {
            loc_A: random.choice(['Dirty', 'Clean']),
            loc_B: random.choice(['Dirty', 'Clean'])
        }
    
    def percept(self, agent):
        return (agent.location, self.status[agent.location])

    def execute_action(self, agent, action):
        self.display_state()
        print("Agent is located at: {}.".format(agent.location))
        print("Current performance score is: {}.".format(agent.performance))
        print("Action to take is: {}.\n".format(action))

        if action == 'Right':
            agent.location = loc_B
            agent.performance -= 1

        elif action == 'Left':
            agent.location = loc_A
            agent.performance -= 1   

        elif action == 'Suck':
            if self.status[agent.location] == 'Dirty':
                agent.performance += 10

            self.status[agent.location] = 'Clean'

    def default_location(self, thing):
        return random.choice([loc_A, loc_B])

    def display_state(self):
        print("Current State of the Environment: {}.".format(self.status))

# Set Up Environment
loc_A, loc_B = (0, 0), (1, 0)
environment = MyTrivialVacuumEnvironment()

#### RANDOM AGENT ####
# Create Random Agent and add to Environment
# random_agent = Agent(program=RandomAgentProgram(['Right', 'Left', 'Suck', 'NoOp']))
# environment.add_thing(random_agent)

# # Simulate and display results.
# environment.run(steps=20)
# print("Final State of the Environment is: {}".format(environment.status))

#### TABLE DRIVEN AGENT ####
# Create State Table
# table = {
#     ((loc_A, 'Clean'), ): 'Right',
#     ((loc_A, 'Dirty'), ): 'Suck',
#     ((loc_B, 'Clean'), ): 'Left',
#     ((loc_B, 'Dirty'), ): 'Suck',
#     ((loc_A, 'Dirty'), (loc_A, 'Clean')): 'Right',
#     ((loc_A, 'Clean'), (loc_B, 'Dirty')): 'Suck',
#     ((loc_B, 'Clean'), (loc_A, 'Dirty')): 'Suck',
#     ((loc_B, 'Dirty'), (loc_B, 'Clean')): 'Left',
#     ((loc_A, 'Dirty'), (loc_A, 'Clean'), (loc_B, 'Dirty')): 'Suck',
#     ((loc_B, 'Dirty'), (loc_B, 'Clean'), (loc_A, 'Dirty')): 'Suck',
# }

# # Create Table Driven Agent and add to Environment.
# table_agent = Agent(program=TableDrivenAgentProgram(table=table))
# environment.add_thing(table_agent)

# # Simulate and display results.
# environment.run(steps=20)
# print("Final State of the Environment is: {}".format(environment.status))

#### SIMPLE REFLEX AGENT ####
# Create program
# def SimpleReflexAgentProgram():
#     def program(percept):
#         loc, status = percept

#         if (status == 'Dirty'):
#             return 'Suck'
#         elif (loc == loc_A):
#             return 'Right'
#         else:
#             return 'Left'
#     return program 

# # Create Simple Reflex Agent and add to Environment.
# simple_reflex_agent = Agent(SimpleReflexAgentProgram())
# environment.add_thing(simple_reflex_agent)

# # Simulate and display results.
# environment.run(steps=20)
# print("Final State of the Environment is: {}".format(environment.status))

#### MODEL BASED REFLEX AGENT ####
# Create Model Based Agent and add to Environment.
# model_based_reflex_agent = ModelBasedVacuumAgent()
# environment.add_thing(model_based_reflex_agent)

# # Simulate and display results.
# environment.run(steps=20)
# print("Final State of the Environment is: {}".format(environment.status))